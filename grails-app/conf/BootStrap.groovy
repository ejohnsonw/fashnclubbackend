/* 
 Copyright (c) 2016 NgeosOne LLC
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 Engineered using http://www.generatron.com/
 [GENERATRON]
 Generator :   System Templates
 Filename:     BootStrap.groovy
 Description:  Authentication Filter, checks for valid access to resources
 Project:      Fashn
 Template: grails-24x/Bootstrap.groovy.vmg
 */



import grails.converters.JSON;

import com.generatron.fashn.*;
import com.generatron.fashn.*;

import org.codehaus.groovy.grails.plugins.springsecurity.ui.*;

class BootStrap {
	def springSecurityService;
	def init = { servletContext ->

		Asset.metaClass.toString = {
			->
			def toString = "";

			return toString;
		};
		Composition.metaClass.toString = {
			->
			def toString = "";
			if(name){
				toString += name.toString();
			}

			return toString;
		};
		Image.metaClass.toString = {
			->
			def toString = "";
			if(description){
				toString += description.toString();
			}
			if(title){
				toString +=" "
				toString += title.toString();
			}

			return toString;
		};
		ImageCollection.metaClass.toString = {
			->
			def toString = "";
			if(name){
				toString += name.toString();
			}

			return toString;
		};
		User.metaClass.toString = {
			->
			def toString = "";
			if(name){
				toString += name.toString();
			}

			return toString;
		};

		JSON.registerObjectMarshaller(Asset) {
			def returnArray = [:]
			//returnArray['class_'] = it.class
			if(it.composition){
				//uncomment this to return whatever is being constructed on the toString
				//def domainArray = [:]
				//domainArray['id']=it.composition.id;
				//domainArray['toString']=it.composition.toString();
				//returnArray['composition'] = domainArray;

				//this returns the full serialized object
				returnArray['composition'] = it.composition;

			}
			if(it.id){
				returnArray['id'] = it.id
			}
			if(it.image){
				//uncomment this to return whatever is being constructed on the toString
				//def domainArray = [:]
				//domainArray['id']=it.image.id;
				//domainArray['toString']=it.image.toString();
				//returnArray['image'] = domainArray;

				//this returns the full serialized object
				returnArray['image'] = it.image;

			}
			if(it.x){
				returnArray['x'] = it.x
			}
			if(it.y){
				returnArray['y'] = it.y
			}
			if(it.z){
				returnArray['z'] = it.z
			}

			returnArray['toString'] = it.toString();
			return returnArray
		}
		JSON.registerObjectMarshaller(Composition) {
			def returnArray = [:]
			//returnArray['class_'] = it.class
			if(it.assets){
				returnArray['assets'] = it.assets
			}
			if(it.id){
				returnArray['id'] = it.id
			}
			if(it.name){
				returnArray['name'] = it.name
			}
			if(it.user){
				//uncomment this to return whatever is being constructed on the toString
				//def domainArray = [:]
				//domainArray['id']=it.user.id;
				//domainArray['toString']=it.user.toString();
				//returnArray['user'] = domainArray;

				//this returns the full serialized object
				returnArray['user'] = it.user;

			}

			returnArray['toString'] = it.toString();
			return returnArray
		}
		JSON.registerObjectMarshaller(Image) {
			def returnArray = [:]
			//returnArray['class_'] = it.class
			if(it.description){
				returnArray['description'] = it.description
			}
			if(it.id){
				returnArray['id'] = it.id
			}
			if(it.imageCollection){
				//uncomment this to return whatever is being constructed on the toString
				//def domainArray = [:]
				//domainArray['id']=it.imageCollection.id;
				//domainArray['toString']=it.imageCollection.toString();
				//returnArray['imageCollection'] = domainArray;

				//this returns the full serialized object
				returnArray['imageCollection'] = it.imageCollection;

			}
			if(it.path){
				returnArray['path'] = it.path
			}
			if(it.title){
				returnArray['title'] = it.title
			}

			returnArray['toString'] = it.toString();
			return returnArray
		}
		JSON.registerObjectMarshaller(ImageCollection) {
			def returnArray = [:]
			//returnArray['class_'] = it.class
			if(it.id){
				returnArray['id'] = it.id
			}
			if(it.images){
				returnArray['images'] = it.images
			}
			if(it.name){
				returnArray['name'] = it.name
			}
			if(it.user){
				//uncomment this to return whatever is being constructed on the toString
				//def domainArray = [:]
				//domainArray['id']=it.user.id;
				//domainArray['toString']=it.user.toString();
				//returnArray['user'] = domainArray;

				//this returns the full serialized object
				returnArray['user'] = it.user;

			}

			returnArray['toString'] = it.toString();
			return returnArray
		}
		JSON.registerObjectMarshaller(User) {
			def returnArray = [:]
			//returnArray['class_'] = it.class
			if(it.compositions){
				returnArray['compositions'] = it.compositions
			}
			if(it.email){
				returnArray['email'] = it.email
			}
			if(it.id){
				returnArray['id'] = it.id
			}
			if(it.imageCollections){
				returnArray['imageCollections'] = it.imageCollections
			}
			if(it.name){
				returnArray['name'] = it.name
			}
			if(it.photo){
				returnArray['photo'] = it.photo
			}

			returnArray['toString'] = it.toString();
			return returnArray
		}
	}
	def destroy = {
	}
}


/* 
 [STATS]
 It would take a person typing  @ 100.0 cpm, 
 approximately 49.79 minutes to type the 4979+ characters in this file.
 */



