/* 
 Copyright (c) 2016 NgeosOne LLC
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 Engineered using http://www.generatron.com/
 [GENERATRON]
 Generator :   System Templates
 Filename:     UrlMappings.groovy
 Description:  URL Mappings
 Project:      Fashn
 Template: grails-24x/URLMappings.groovy.vmg
 */

class UrlMappings {
	static mappings = {
		"/$controller/$action?/$id?(.$format)?"{ constraints { // apply constraints here
			} }

		"/"(view:"/index")
		"500"(view:'/error')

		//routes for Asset
		//using POST:update instead of put, because PUT multiform with files is not interpreted correctly by spring framework
		"/api/asset/$id"(controller:"AssetApi"){
			action = [GET:"retrieve", POST:"update", DELETE:"delete"]
		}
		"/api/asset"(controller:"AssetApi"){
			action = [GET:"index", POST:"create"]
		}


		//routes for Composition
		//using POST:update instead of put, because PUT multiform with files is not interpreted correctly by spring framework
		"/api/composition/$id"(controller:"CompositionApi"){
			action = [GET:"retrieve", POST:"update", DELETE:"delete"]
		}
		"/api/composition"(controller:"CompositionApi"){
			action = [GET:"index", POST:"create"]
		}

		"/api/composition/$id/addAssetToComposition"(controller:"CompositionApi",action:"addAssetToComposition")
		"/api/composition/$id/removeAssetFromComposition"(controller:"CompositionApi",action:"removeAssetFromComposition")

		//routes for Image
		//using POST:update instead of put, because PUT multiform with files is not interpreted correctly by spring framework
		"/api/image/$id"(controller:"ImageApi"){
			action = [GET:"retrieve", POST:"update", DELETE:"delete"]
		}
		"/api/image"(controller:"ImageApi"){
			action = [GET:"index", POST:"create"]
		}


		//routes for ImageCollection
		//using POST:update instead of put, because PUT multiform with files is not interpreted correctly by spring framework
		"/api/imageCollection/$id"(controller:"ImageCollectionApi"){
			action = [GET:"retrieve", POST:"update", DELETE:"delete"]
		}
		"/api/imageCollection"(controller:"ImageCollectionApi"){
			action = [GET:"index", POST:"create"]
		}

		"/api/imageCollection/$id/addImageToImageCollection"(controller:"ImageCollectionApi",action:"addImageToImageCollection")
		"/api/imageCollection/$id/removeImageFromImageCollection"(controller:"ImageCollectionApi",action:"removeImageFromImageCollection")

		//routes for User
		//using POST:update instead of put, because PUT multiform with files is not interpreted correctly by spring framework
		"/api/user/$id"(controller:"UserApi"){
			action = [GET:"retrieve", POST:"update", DELETE:"delete"]
		}
		"/api/user"(controller:"UserApi"){
			action = [GET:"index", POST:"create"]
		}

		"/api/user/$id/addCompositionToUser"(controller:"UserApi",action:"addCompositionToUser")
		"/api/user/$id/removeCompositionFromUser"(controller:"UserApi",action:"removeCompositionFromUser")
		"/api/user/$id/addImageCollectionToUser"(controller:"UserApi",action:"addImageCollectionToUser")
		"/api/user/$id/removeImageCollectionFromUser"(controller:"UserApi",action:"removeImageCollectionFromUser")


	}
}

/* 
 [STATS]
 It would take a person typing  @ 100.0 cpm, 
 approximately 28.74 minutes to type the 2874+ characters in this file.
 */



