/* 
 Copyright (c) 2016 NgeosOne LLC
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 Engineered using http://www.generatron.com/
 [GENERATRON]
 Generator :   System Templates
 Filename:     ImageCollectionService.groovy
 Description:  creates a controller to process all api requests
 Project:      Fashn
 Template: grails-24x/EntityService.groovy.vm
 */
package  com.generatron.fashn

import com.generatron.fashn.*;

import grails.converters.JSON;
import org.springframework.dao.DataIntegrityViolationException;

import com.generatron.annotations.*
@Generatron(explore=false, generate=false)

class ImageCollectionService {
	public static generatron=true
	/**
	 * 
	 * ImageCollectionService for ImageCollection
	 * 
	 * 
	 */


	def addImageCollection(ImageCollection imageCollection) {
		return imageCollection.save(flush:true)
	}

	def updateImageCollection(ImageCollection imageCollection) {
		return imageCollection.save(flush:true)
	}

	def deleteImageCollection(Long id) {
		def e = ImageCollection.findById(id);
		return e.delete(flush:true);
	}

	def imageCollection(Long id) {
		def e = ImageCollection.findById(id);
		if(e){
			return e;
		}else{
			return null;
		}
	}


	def imageCollections() {
		def results = ImageCollection.list();
		return results;
	}



	//From relations
	//--->images
	def addImageToImageCollection(Long id,Image obj) {
		def e = ImageCollection.findById(id);
		e.addToImages(obj);
		return e.save(flush:true);
	}

	def removeImageFromImageCollection(Long id,Image obj){
		def e = ImageCollection.findById(id);
		e.removeFromImages(obj);
		return e.save(flush:true)
	}
	//--->user




}




/* 
 [STATS]
 It would take a person typing  @ 100.0 cpm, 
 approximately 13.12 minutes to type the 1312+ characters in this file.
 */



