/* 
 Copyright (c) 2016 NgeosOne LLC
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 Engineered using http://www.generatron.com/
 [GENERATRON]
 Generator :   System Templates
 Filename:     UserService.groovy
 Description:  creates a controller to process all api requests
 Project:      Fashn
 Template: grails-24x/EntityService.groovy.vm
 */
package  com.generatron.fashn

import com.generatron.fashn.*;

import grails.converters.JSON;
import org.springframework.dao.DataIntegrityViolationException;

import com.generatron.annotations.*
@Generatron(explore=false, generate=false)

class UserService {
	public static generatron=true
	/**
	 * 
	 * UserService for User
	 * 
	 * 
	 */


	def addUser(User user) {
		return user.save(flush:true)
	}

	def updateUser(User user) {
		return user.save(flush:true)
	}

	def deleteUser(Long id) {
		def e = User.findById(id);
		return e.delete(flush:true);
	}

	def user(Long id) {
		def e = User.findById(id);
		if(e){
			return e;
		}else{
			return null;
		}
	}


	def users() {
		def results = User.list();
		return results;
	}



	//From relations
	//--->compositions
	def addCompositionToUser(Long id,Composition obj) {
		def e = User.findById(id);
		e.addToCompositions(obj);
		return e.save(flush:true);
	}

	def removeCompositionFromUser(Long id,Composition obj){
		def e = User.findById(id);
		e.removeFromCompositions(obj);
		return e.save(flush:true)
	}
	//--->imageCollections
	def addImageCollectionToUser(Long id,ImageCollection obj) {
		def e = User.findById(id);
		e.addToImageCollections(obj);
		return e.save(flush:true);
	}

	def removeImageCollectionFromUser(Long id,ImageCollection obj){
		def e = User.findById(id);
		e.removeFromImageCollections(obj);
		return e.save(flush:true)
	}




}




/* 
 [STATS]
 It would take a person typing  @ 100.0 cpm, 
 approximately 14.65 minutes to type the 1465+ characters in this file.
 */



