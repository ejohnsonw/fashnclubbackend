/* 
 Copyright (c) 2016 NgeosOne LLC
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 Engineered using http://www.generatron.com/
 [GENERATRON]
 Generator :   System Templates
 Filename:     CompositionApiController.groovy
 Description:  creates a controller to process all api requests
 Project:      Fashn
 Template: grails-24x/EntityApiController.groovy.vm
 */

package  com.generatron.fashn

import com.generatron.fashn.*;

import grails.converters.JSON;
import org.springframework.dao.DataIntegrityViolationException;

import com.generatron.annotations.*
@Generatron(explore=false, generate=false)
class CompositionApiController {
	public static generatron=true
	def compositionService;

	/**
	 * 
	 * Entity: Composition
	 * 
	 * 
	 */

	//---->>
	//---->>


	def create() {
		Composition composition = null;
		if(request.JSON){
			composition = new  Composition(request.JSON);
		}else{
			def _model = JSON.parse(params);
			composition = new Composition(_model);
		}


		if(!composition.validate()){
			response.status = 500;
			render composition.errors as JSON;
			return;
		}

		def result = compositionService.addComposition(composition)
		render result as JSON;
	}

	def update(Long id) {
		Composition composition = Composition.findById(id);;
		if(composition == null){
			response.status = 500;
			render "[{error:'Not Found'}]";
		}
		if(request.JSON){
			composition.properties  = request.JSON;
		}else{
			def _model = JSON.parse(params);
			composition = new Composition(_model);
		}


		if(!composition.validate()){
			response.status = 500;
			render composition.errors as JSON;

			return;
		}
		def result = compositionService.updateComposition(composition)
		render result as JSON;
	}


	def delete(Long id) {
		def result = compositionService.deleteComposition(id)
		//TODO: implement delete of file
		render(status:200,text:"Deleted");
	}


	def retrieve(Long id) {
		def result = compositionService.composition(id)
		if(result){
			render result as  JSON;
		}else{
			render(status:500,text:"Could not retrieve");
		}
	}


	def index() {
		def results = Composition.list();
		render results as JSON;
	}

	def addAssetsToComposition(Long id) {
		def jsonObject = request.JSON;
		def result = compositionService.addAssetsToComposition(id,jsonObject)
		if(result){
			render result as JSON;
		}else{
			render(status:500,text:ex.message);
		}
	}
	def removeAssetsFromComposition(Long id){
		def e = Composition.findById(id);
		def jsonObject = request.JSON;
		def result = compositionService.removeAssetsFromComposition(id,jsonObject)
		if(result){
			render result as JSON;
		}else{
			render(status:500,text:ex.message);
		}
	}

	private String saveFile(Object f, String fieldName){

		//TODO: everytime the app is deployed, uploaded files are lost use something other than servletContext.getRealPath("/");
		def appDir = servletContext.getRealPath("/")
		File dirDest = new File(appDir,"/uploads/Composition/"+fieldName+"/")
		if(!dirDest.exists()){
			dirDest.mkdirs();
		}
		File fileDest = new File(appDir,"uploads/Composition/"+fieldName+"/"+f.getOriginalFilename())
		f.transferTo(fileDest)
		return fileDest.getAbsolutePath();
	}


}







/* 
 [STATS]
 It would take a person typing  @ 100.0 cpm, 
 approximately 28.71 minutes to type the 2871+ characters in this file.
 */



