/* 
 Copyright (c) 2016 NgeosOne LLC
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 Engineered using http://www.generatron.com/
 [GENERATRON]
 Generator :   System Templates
 Filename:     ImageApiController.groovy
 Description:  creates a controller to process all api requests
 Project:      Fashn
 Template: grails-24x/EntityApiController.groovy.vm
 */

package  com.generatron.fashn

import com.generatron.fashn.*;

import grails.converters.JSON;
import org.springframework.dao.DataIntegrityViolationException;

import com.generatron.annotations.*
@Generatron(explore=false, generate=false)
class ImageApiController {
	public static generatron=true
	def imageService;

	/**
	 * 
	 * Entity: Image
	 * 
	 * 
	 */

	//---->>
	//---->>


	def create() {
		Image image = null;
		if(request.JSON){
			image = new  Image(request.JSON);
		}else{
			def _model = JSON.parse(params);
			image = new Image(_model);
		}


		if(!image.validate()){
			response.status = 500;
			render image.errors as JSON;
			return;
		}

		def result = imageService.addImage(image)
		render result as JSON;
	}

	def update(Long id) {
		Image image = Image.findById(id);;
		if(image == null){
			response.status = 500;
			render "[{error:'Not Found'}]";
		}
		if(request.JSON){
			image.properties  = request.JSON;
		}else{
			def _model = JSON.parse(params);
			image = new Image(_model);
		}


		if(!image.validate()){
			response.status = 500;
			render image.errors as JSON;

			return;
		}
		def result = imageService.updateImage(image)
		render result as JSON;
	}


	def delete(Long id) {
		def result = imageService.deleteImage(id)
		//TODO: implement delete of file
		render(status:200,text:"Deleted");
	}


	def retrieve(Long id) {
		def result = imageService.image(id)
		if(result){
			render result as  JSON;
		}else{
			render(status:500,text:"Could not retrieve");
		}
	}


	def index() {
		def results = Image.list();
		render results as JSON;
	}


	private String saveFile(Object f, String fieldName){

		//TODO: everytime the app is deployed, uploaded files are lost use something other than servletContext.getRealPath("/");
		def appDir = servletContext.getRealPath("/")
		File dirDest = new File(appDir,"/uploads/Image/"+fieldName+"/")
		if(!dirDest.exists()){
			dirDest.mkdirs();
		}
		File fileDest = new File(appDir,"uploads/Image/"+fieldName+"/"+f.getOriginalFilename())
		f.transferTo(fileDest)
		return fileDest.getAbsolutePath();
	}


}







/* 
 [STATS]
 It would take a person typing  @ 100.0 cpm, 
 approximately 21.18 minutes to type the 2118+ characters in this file.
 */



